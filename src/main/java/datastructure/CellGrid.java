package datastructure;
import java.util.Iterator;
import cellular.CellState;
import java.util.ArrayList;

public class CellGrid implements IGrid{

    int rows;
    int columns;
    CellState initiaState;
    CellState[][] grid;
    //private ArrayList<CellState[][]> grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];
        
        
        for(int r = 0; r < numRows(); r++){
            for(int c = 0; c < numColumns(); c++){
                grid[r][c] = initialState;
            }
        }
    }

	

    @Override
    public int numRows() {
        
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row > numRows()) {
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column > numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row > numRows()) {
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column > numColumns()) {
            throw new IndexOutOfBoundsException();
        }

        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        
        CellGrid newGrid = new CellGrid(this.numRows(), this.numColumns(), CellState.DEAD);
        //newGrid = new CellState[this.numRows()][this.numColumns()];
        //newGrid.grid = this.grid;
        for(int r = 0; r < this.numRows(); r++){
            for(int c = 0; c < this.numColumns(); c++){
                newGrid.grid[r][c] = grid[r][c];
            }
        }
        return newGrid;
    }
    
}
